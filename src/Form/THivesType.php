<?php

namespace App\Form;

use App\Entity\THives;
use App\Entity\TTypes;
use App\Entity\TStates;
use App\Entity\TFormats;
use App\Entity\TApiaries;
use App\Entity\TCategories;
use App\Entity\TMateriels;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
// use Symfony\Component\DependencyInjection\Loader\Configurator\security;

class THivesType extends AbstractType
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reference', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Référence de la ruche'
            ])
            ->add('purchaseDate', DateType::class, [
                'format' => 'dd-MM-yyyy',
                'label' => 'Date d\'achat',
                "years" => range(date('Y'), date('Y') - 125)
            ])
            ->add('noteText', TextareaType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Note',
            ])
            ->add('fkMaterial', EntityType::class, [
                'class' => TMateriels::class,
                'label' => 'Matière principale',
                'attr' => ['class' => 'form-select'],
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('materiel')
                        ->where('materiel.fkUser = :userId')
                        ->orderBy('materiel.label', 'ASC')
                        ->setParameter('userId', $this->security->getUser()->getId());
                },
                'choice_label' => 'label'
            ])
            ->add('fkType', EntityType::class, [
                'class' => TTypes::class,
                'label' => 'Type de ruche',
                'attr' => ['class' => 'form-select'],
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('type')
                        ->where('type.fkUser = :userId')
                        ->orderBy('type.label', 'ASC')
                        ->setParameter('userId', $this->security->getUser()->getId());
                },
                'choice_label' => 'label'
            ])
            ->add('fkApiary', EntityType::class, [
                'class' => TApiaries::class,
                'label' => 'Rucher cible',
                'attr' => ['class' => 'form-select'],
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('apiary')
                        ->where('apiary.fkUser = :userId')
                        ->orderBy('apiary.name', 'ASC')
                        ->setParameter('userId', $this->security->getUser()->getId());
                },
                'choice_label' => 'name'
            ])
            ->add('fkFormat', EntityType::class, [
                'class' => TFormats::class,
                'label' => 'Format de ruche',
                'attr' => ['class' => 'form-select'],
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('format')
                        ->where('format.fkUser = :userId')
                        ->orderBy('format.label', 'ASC')
                        ->setParameter('userId', $this->security->getUser()->getId());
                },
                'choice_label' => 'label'
            ])
            ->add('fkState', EntityType::class, [
                'class' => TStates::class,
                'label' => 'Etat général',
                'attr' => ['class' => 'form-select'],
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('state')
                        ->where('state.fkUser = :userId')
                        ->orderBy('state.label', 'ASC')
                        ->setParameter('userId', $this->security->getUser()->getId());
                },
                'choice_label' => 'label'
            ])
            ->add('fkCategory', EntityType::class, [
                'class' => TCategories::class,
                'label' => 'Catégorie de ruche',
                'attr' => ['class' => 'form-select'],
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('category')
                        ->where('category.fkUser = :userId')
                        ->orderBy('category.label', 'ASC')
                        ->setParameter('userId', $this->security->getUser()->getId());
                },
                'choice_label' => 'label'
            ])
            // ->add('idQueens', CollectionType::class)
            // ->add('idSwarms', CollectionType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => THives::class,
        ]);
    }
}
