<?php

namespace App\Form;

use App\Entity\TUserInfoPhone;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TUserInfoPhoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('phone', TextType::class, [
                'label' => 'Saisir un numéro de téléphone',
                'attr' => ['class' => 'form-control']
            ]);
        // ->add('userIdPhoneAccount')

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TUserInfoPhone::class,
        ]);
    }
}
