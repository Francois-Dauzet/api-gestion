<?php

namespace App\Form;

use App\Entity\TQueens;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class TQueensType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reference')
            ->add('fertilizationDate', DateType::class, [
                'format' => 'dd-MM-yyyy',
                "years" => range(date('Y'), date('Y') - 125)
            ])
            ->add('marking')
            ->add('orphan')
            ->add('fkBreed')
            ->add('fkIdHives')
            ->add('idNotes');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TQueens::class,
        ]);
    }
}
