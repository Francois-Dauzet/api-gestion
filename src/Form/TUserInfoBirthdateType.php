<?php

namespace App\Form;

use App\Entity\TUserInfoBirthdate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class TUserInfoBirthdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('birthdate', BirthdayType::class, [
                'format' => 'dd-MM-yyyy',
                "years" => range(date('Y'), date('Y') - 125),
                'label' => 'Choisir une date',
                'attr' => ['class' => 'form-control']
            ])
            // ->add('userIdBirthdateAccount')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TUserInfoBirthdate::class,
        ]);
    }
}
