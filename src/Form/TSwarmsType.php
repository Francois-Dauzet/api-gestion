<?php

namespace App\Form;

use App\Entity\TSwarms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TSwarmsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fkPopulation')
            ->add('fkTemperament')
            ->add('fkIdHives')
            ->add('fkIdDiseases')
            ->add('idNotes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TSwarms::class,
        ]);
    }
}
