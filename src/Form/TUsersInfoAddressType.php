<?php

namespace App\Form;

use App\Entity\TPcTown;
use App\Entity\TUsersInfoAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class TUsersInfoAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('streetNumber', TextType::class, [
                'required' => false,
                'label' => 'Numéro de rue',
                'attr' => ['class' => 'form-control']
            ])
            ->add('streetName', TextType::class, [
                'label' => 'Nom de rue',
                'attr' => ['class' => 'form-control']
            ])
            ->add('postalCode', TextType::class, [
                'label' => 'Code postal',
                'attr' => ['class' => 'form-control']
            ])
            ->add('town', TextType::class, [
                'label' => 'Ville',
                'attr' => ['class' => 'form-control']
            ])
            // ->add('userIdAddressAccount')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TUsersInfoAddress::class,
        ]);
    }
}
